Дано два масиви розмірністю 3x3, які заповнені випадковими цілими числами в діапазоні [-10, 10].

```
var array_1, array_2;
```

Знайти:

```
var sum_1_2; // array_1 + array_2
var sum_2_1; // array_2 + array_1
var diff_1_2; // array_1 - array_2
var diff_2_1; // array_2 - array_1
var product_1_2; // array_1 * array_2
var product_2_1; // array_2 * array_1
var determinant_1; // |array_1|
var determinant_2; // |array_2|
var transpose_1; // transpose array_1
var transpose_2; // transpose array_2
```
